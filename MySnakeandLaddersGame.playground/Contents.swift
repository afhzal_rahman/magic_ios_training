//: Playground - noun: a place where people can play

import Foundation

print("\nWelcome to Snake and Ladders")

class Trick {
    var type: String = ""
    var start: Int = 0
    var end: Int = 0
    
    init (type: String, start: Int, end: Int){
        self.type = type
        self.start = start
        self.end = end
    }
}

//setup gameboard with tricks
var snake1: Trick = Trick(type: "Snake", start: 21, end: 7)
var snake2: Trick = Trick(type: "Snake", start: 16, end: 3)
var snake3: Trick = Trick(type: "Snake", start: 9, end: 4)
var snake4: Trick = Trick(type: "Snake", start: 13, end: 8)
var ladder1: Trick = Trick(type: "Ladder", start: 11, end: 24)
var ladder2: Trick = Trick(type: "Ladder", start: 5, end: 12)

var tricks: [Trick] = [snake1, snake2, ladder1, ladder2, snake3, snake4]

//class Board {
//    var maxBoxes: Int = 25
//    var tricks: [Trick] = [snake1, snake2, ladder1, ladder2, snake3, snake4]
//}
//var myGameBoard: Board = Board()

class Player {
    var color: String = ""
    var position: Int = 0
    
    func showColor() {
        print("My color is \(color) and I am now at box \(position)")
    }
    
    func setNewPosition(newPosition:Int) {
        self.position = newPosition;
    }

    func getPosition() -> Int {
        return self.position;
    }
}

var playerOne: Player = Player()
playerOne.color = "blue"
playerOne.showColor()

var playerTwo: Player = Player()
playerTwo.color = "red"
playerTwo.showColor()

func rollDice() -> Int {
    
    print("Throwing dice......")
    
    switch (Int(arc4random_uniform(6)+1)) {
    case 1:
        print("You got a 1")
        return 1;
    case 2:
        print("You got a 2")
        return 2;
    case 3:
        print("You got a 3")
        return 3;
    case 4:
        print("You got a 4")
        return 4;
    case 5:
        print("You got a 5")
        return 5;
    case 6:
        print("You got a 6")
        return 6;
    default:
        print("You got a 0")
        return 0;
    }
}

func checkForTrick(currentPosition: Int, moves: Int) -> Int {
    
    var newPosition: Int = 0
    newPosition = currentPosition + moves;
    
    for Trick in tricks {
        
        if(newPosition == Trick.start) {
            print("You just ran into a \(Trick.type) and will be moved from \(Trick.start) to \(Trick.end)!")
            newPosition = Trick.end
        }
    }
    
    return newPosition
}

var winFlag: Bool = true;
var round: Int = 1;

while(winFlag) {
    
    print("\nRound \(round++)")
    
    // take turns moving until a player reaches/exceeds the end point
    playerOne.setNewPosition(checkForTrick(playerOne.getPosition(), moves: rollDice()))
    playerOne.showColor()
    
    playerTwo.setNewPosition(checkForTrick(playerTwo.getPosition(), moves: rollDice()))
    playerTwo.showColor()

    if(playerOne.position >= 25) {
        winFlag = false;
        print("The winner is \(playerOne.color)")
        
    } else if(playerTwo.position >= 25) {
        winFlag = false;
        print("\nThe winner is \(playerTwo.color)")
    }

    
}

