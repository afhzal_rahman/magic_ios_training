//
//  HelloWorldViewController.swift
//  MyOwnHelloWorld
//
//  Created by Afhzal Abdul Rahman on 10/31/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import UIKit

class HelloWorldViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.view.backgroundColor = UIColor.grayColor()
        
/*      let screenLabel: UILabel = UILabel(frame: CGRect(x: 20, y: 40, width: 200, height: 40))
        screenLabel.text = "Welcome to My Hello World"
        screenLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(screenLabel)
        
        // Hello button
        let helloButton: UIButton = UIButton(frame: CGRect(x: 20, y: 100, width: 80, height: 40))
        helloButton.backgroundColor = UIColor.redColor()
        helloButton.setTitle("Hello", forState: .Normal)
        self.view.addSubview(helloButton)

        // Bye button
        let byeButton: UIButton = UIButton(frame: CGRect(x: 120, y: 100, width: 80, height: 40))
        byeButton.backgroundColor = UIColor.blueColor()
        byeButton.setTitle("Bye", forState: .Normal)

        helloButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        byeButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(byeButton)
*/
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onPress(sender: UIButton) {
        let alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "Your confirmation is required!";
        alertView.message = "You pressed the \(sender.titleLabel!.text!) button";
        alertView.show();
    }

    @IBAction func changeAppBG(sender: UISegmentedControl) {
        
        var color: String?
        
        if (sender.selectedSegmentIndex == 0) {
            self.view.backgroundColor = UIColor.whiteColor()
            color = "white"
        } else if (sender.selectedSegmentIndex == 1){
            self.view.backgroundColor = UIColor.grayColor()
            color = "gray"
        } else {
            self.view.backgroundColor = UIColor.blueColor()
            color = "blue"
        }

        let alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "Color Selected";
        alertView.message = "You selected the color \(color!) button";
        alertView.show();
 
    }
    
}
